Rails.application.routes.draw do
  devise_for :users, skip: [ :registrations, :sessions ]
  as :user do
    get '/login' => 'devise/sessions#new', :as => :new_user_session
    post '/login' => 'devise/sessions#create', :as => :user_session
    get '/logout' => 'devise/sessions#destroy', :as => :destroy_user_session
    get '/register' => 'couples#new', as: :new_couple_registration
    post '/register' => 'couples#create', as: :couple_registration
  end

  resources :stories, except: [ 'index' ]
  resources :couples, only: [ :show, :edit, :update ]

  namespace :home do
    get :index
  end

  post '/upload_images' => 'home#upload_images'

  root to: 'home#index'
end
