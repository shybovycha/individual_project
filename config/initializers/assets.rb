# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( couples.js couple_sign_up.js editor.js stories.js utilities.js )
Rails.application.config.assets.precompile += %w( couples.css couple_sign_up.css editor.css stories.css utilities.css )
