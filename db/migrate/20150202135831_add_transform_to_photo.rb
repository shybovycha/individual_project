class AddTransformToPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :source_x, :integer, default: 0
    add_column :photos, :source_y, :integer, default: 0
    add_column :photos, :scale_factor, :float, default: 1.0
  end
end
