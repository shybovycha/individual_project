class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :title
      t.attachment :icon

      t.timestamps null: false
    end
  end
end
