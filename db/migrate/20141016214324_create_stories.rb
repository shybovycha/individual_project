class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.string :title
      t.text :body
      t.integer :couple_id
      t.integer :author_id

      t.timestamps null: false
    end

    create_table :stories_tags, id: false do |t|
      t.integer :story_id
      t.integer :tag_id
    end
  end
end
