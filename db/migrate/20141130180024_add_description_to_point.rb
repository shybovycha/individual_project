class AddDescriptionToPoint < ActiveRecord::Migration
  def change
    add_column :points, :description, :text
    add_column :points, :title, :string
  end
end
