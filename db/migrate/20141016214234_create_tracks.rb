class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :color
      t.integer :story_id

      t.timestamps null: false
    end
  end
end
