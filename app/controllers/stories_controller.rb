class StoriesController < ApplicationController
  before_action :authenticate_user!, except: [ :show ]
  before_action :find_story, except: [ :create, :new ]

  layout 'stories', except: [ :new, :edit ]
  layout 'editor', only: [ :new, :edit ]

  def show
    @tracks = @story.present_tracks
  end

  def new
    @story = Story.new
  end

  def create
    @story = Story.new story_params

    errors = []

    @story.couple = current_user.couple
    @story.author = current_user

    unless @story.save
      errors += @story.errors.full_messages
    end

    track = Track.new

    unless track.save
      errors += track.errors.full_messages
    end

    points_params[:points].each do |point_params|
      point = Point.new point_params.symbolize_keys

      unless point.save
        errors += point.errors.full_messages
      end

      track.points << point
    end

    @story.tracks << track

    if errors.empty?
      redirect_to @story, flash: { info: "Story created" }
    else
      redirect_to :back, flash: { alert: "Could not create story because of: #{ errors.join(', ') }" }
    end
  end

  def destroy
    @couple = current_user.couple

    if @story.destroy
      redirect_to couple_path(@couple), flash: { info: "Story removed" }
    else
      redirect_to :back, flash: { alert: "Could not remove this story" }
    end
  end

  def edit
  end

  def update
    errors = []

    errors += @story.errors.full_messages unless @story.update_attributes story_params

    @story.tracks.first.points.destroy_all

    points_params[:points].each do |point_params|
      point = Point.new point_params.symbolize_keys

      unless point.save
        errors += point.errors.full_messages
      end

      @story.tracks.first.points << point
    end

    if errors.empty?
      redirect_to story_path(@story), flash: { info: "Story updated" }
    else
      redirect_to :back, flash: { alert: errors.join(', ') }
    end
  end

  protected

  def find_story
    @story = Story.find(params[:id])
  end

  def story_params
    params.require(:story).permit(:title, :body)
  end

  def points_params
    params.permit({ :points => [ [ :title, :description, :lat, :lng ] ] })
  end
end
