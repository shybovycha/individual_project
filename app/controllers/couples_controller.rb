class CouplesController < ApplicationController
  layout 'couples', except: [ :new, :edit ]
  layout 'couple_sign_up', only: [ :new, :edit ]

  def new
    @couple = Couple.new
  end

  def create
    @couple = Couple.new
    @he = User.new his_params
    @she = User.new her_params

    @user_to_log_in = (rand(100) > 50) ? @he : @she

    @couple.photo = Photo.find_by(id: their_photo_param)
    @he.photo = Photo.find_by(id: his_photo_param)
    @she.photo = Photo.find_by(id: her_photo_param)

    if @couple.photo
      @couple.photo.update couple_photo_params
    end

    if @he.save and @she.save and @couple.he = @he and @couple.she = @she and @couple.save
      sign_in(User, @user_to_log_in)
      redirect_to couple_path(@couple)
    else
      render "new"
    end
  end

  def edit
    @couple = Couple.find(params[:id])
  end

  def update
    # required for settings form to submit when password is left blank
    if params[:his][:password].blank? && params[:his][:password_confirmation].blank?
      params[:his].delete(:password)
      params[:his].delete(:password_confirmation)
    end

    if params[:her][:password].blank? && params[:her][:password_confirmation].blank?
      params[:her].delete(:password)
      params[:her].delete(:password_confirmation)
    end

    @user = current_user
    @couple = Couple.find(params[:id])
    @he = @couple.he
    @she = @couple.she

    if params[:his][:photo_id].present?
      @he.photo = Photo.find(params[:his][:photo_id])
      params[:his].delete :photo_id
    end

    if params[:her][:photo_id].present?
      @she.photo = Photo.find(params[:her][:photo_id])
      params[:her].delete :photo_id
    end

    if params[:their][:photo_id].present?
      @couple.photo = Photo.find(params[:their][:photo_id])
      params[:their].delete :photo_id
    end

    if @couple.photo
      @couple.photo.update couple_photo_params
    end

    @he.update_attributes his_params
    @she.update_attributes her_params

    if @he.save and @she.save and @couple.save
      # Sign in the user bypassing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to couple_path(@couple), { flash: { notice: 'Your settings are saved' } }
    else
      render "edit"
    end
  end

  def show
    @couple = Couple.find(params[:id])

    @tracks = @couple.stories.map do |story|
      story.present_tracks
    end.flatten(1)
  end

  protected

  def user_params
    [ :name, :email, :password, :password_confirmation ]
  end

  def his_params
    params.permit(:his => user_params)[:his]
  end

  def her_params
    params.permit(:her => user_params)[:her]
  end

  def his_photo_param
    params.permit(:his => [:photo_id])[:his][:photo_id]
  rescue
    nil
  end

  def her_photo_param
    params.permit(:her => [:photo_id])[:her][:photo_id]
  rescue
    nil
  end

  def their_photo_param
    params.permit(:their => [:photo_id])[:their][:photo_id]
  rescue
    nil
  end

  def couple_photo_params
    params.permit(:their => [:source_x, :source_y, :scale_factor])[:their]
  end

  def couple_params
    params.permit(:their => [:photo_id, :source_x, :source_y, :scale_factor])[:their]
  end

  def after_update_path_for(couple)
    couple_path(couple)
  end
end
