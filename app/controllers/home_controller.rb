class HomeController < ApplicationController
  #before_filter :authenticate_user, only: [ :upload_images ]
  layout 'stories'

  def index
    if user_signed_in?
      redirect_to couple_path(current_user.couple)
    end
  end

  def upload_images
    images = []
    session[:photos] ||= []

    params[:images].each do |image|
      photo = Photo.create image: image.tempfile

      session[:photos] << photo.id

      images << { url: photo.image.url(:medium), id: photo.id }
      images << { url: photo.image.url(:original), id: photo.id }
    end

    render json: { images: images }.to_json
  end
end
