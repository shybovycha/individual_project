class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :couple

  #has_attached_file :photo, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  #validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/

  has_one :photo, as: :imageable
  has_many :stories, foreign_key: :author_id

  validates :name, presence: true
  #validates :photo, presence: true

  def is_he?
    couple.he == self
  end

  def is_she?
    couple.she == self
  end

  def couple
    Couple.where('his_id = :id OR her_id = :id', id: self.id).first
  end

  def avatar(image_size = :thumb)
    self.photo.image.url image_size if self.photo.present?
  end

  def couple_name
    couple.name
  end
end
