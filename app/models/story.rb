class Story < ActiveRecord::Base
  has_many :tracks
  has_many :tags
  has_many :photos, as: :imageable

  belongs_to :couple
  belongs_to :author, class_name: 'User'

  # validates :tags, length: { minimum: 1 }

  def short_body
    body.gsub(/\A(.*)<hr\s*\/>.*\z/, '\1')
  end

  def present_tracks
    self.tracks.map do |track|
      points = track.points.map do |point|
        {
            title: point.title,
            description: point.description,
            lat: point.lat,
            lng: point.lng
        }
      end

      {
          color: track.color,
          points: points
      }
    end
  end
end
