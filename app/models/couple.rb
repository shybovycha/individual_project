class Couple < ActiveRecord::Base
  devise :registerable

  belongs_to :he, class_name: "User", foreign_key: :his_id
  belongs_to :she, class_name: "User", foreign_key: :her_id

  alias_attribute :his, :he
  alias_attribute :her, :she

  has_many :stories

  has_one :photo, as: :imageable

  validates :he, presence: true
  validates :she, presence: true

  def name
    "#{ he.name } and #{ she.name }"
  end

  def avatar(image_size = :thumb)
    self.photo.image.url image_size if self.photo.present?
  end
end
