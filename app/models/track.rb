class Track < ActiveRecord::Base
  has_many :points
  belongs_to :story

  before_save :pick_color

  protected

  def pick_color
    self.color = ['#f55', '#b00', '#0b0', '#00b'].sample
  end
end
