class Tag < ActiveRecord::Base
  belongs_to :story

  has_attached_file :icon, :styles => { :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :icon, :content_type => /\Aimage\/.*\Z/
end
