class Point < ActiveRecord::Base
  belongs_to :track

  has_one :photo, as: :imageable

  validates :lat, presence: true
  validates :lng, presence: true
end
