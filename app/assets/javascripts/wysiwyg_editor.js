function initWysiwyg(wysiwyg_config) {
    if (document.querySelectorAll('.editor-wrapper').length < 1) {
        console.error('Could not init WYSIWYG editor because no proper markup is found');
        return;
    }

    var buttons = document.querySelectorAll('.toolbar .button');

    if (typeof(wysiwyg_config) == 'undefined' || !wysiwyg_config) {
        wysiwyg_config = {
            maxFileSize: (1024 * 1024 * 2),
            allowedFileTypes: [ 'image/png', 'image/gif', 'image/jpeg' ],
            fileUploadUrl: '/upload_images'
        };
    }

    [].forEach.call(buttons, function(elt) {
        elt.addEventListener('click', function(evt) {
            evt.preventDefault();

            var command = elt.dataset['command'];
            var selected = document.getSelection();

            switch (command) {
                case 'createLink':
                    document.execCommand(command, false, selected);
                    break;

                default:
                    document.execCommand(command, false, null);
            }
        });
    });

    var draggable = document.querySelectorAll('.imagebox-wrapper .image-container');
    var dropper = document.querySelector('.dropper');
    var editor = document.querySelector('.editor');

    [].forEach.call(draggable, function(elt) {
        if (elt.classList.contains('dropper'))
            return;

        elt.setAttribute('draggable', 'true');

        elt.addEventListener('dragstart', function(e) {
            this.classList.add('dragged');
            e.dataTransfer.effectAllowed = 'copy';
            window.draggingElement = this;

            var image = this.querySelector('img'),
                imageUrl = image.getAttribute('url'),
                imageAlt = image.getAttribute('alt'),
                imageHtml = '<img src="' + imageUrl + '" alt="' + imageAlt + '" />'

            // set image
            e.dataTransfer.setData('Text', '');
            //e.dataTransfer.setData('text/html', imageHtml);
        }, false);

        elt.addEventListener('drag', function(e) {
        }, false);
    });

    dropper.ondragover = function () { return false; };

    dropper.ondragend = function () { return false; };

    document.addEventListener('drop', function(e) {
        var maxFileSize = wysiwyg_config.maxFileSize,
            allowedFileTypes = wysiwyg_config.allowedFileTypes,
            fileUploadUrl = wysiwyg_config.fileUploadUrl;

        if (e.target == editor || e.target.parentNode == editor) {
            // window.draggingElement.classList.remove('dragged');
        } else if (e.target == dropper || e.target.parentNode == dropper) {
            e.preventDefault();

            var formData = new FormData();

            if (e.dataTransfer.files.length > 0) {
                [].forEach.call(e.dataTransfer.files,  function(file) {
                    if (allowedFileTypes.indexOf(file.type) < 0) {
                        console.error("could not upload file of type ", file.type, " while allowed types are only ", allowedTypes);
                        return false;
                    }

                    if (file.size > maxFileSize) {
                        console.error("could not upload file because its size exceeds the maximum allowed one - ", file.size, " out of ", maxFileSize);
                        return false;
                    }

                    formData.append('images[]', file);
                });

                var request = new XMLHttpRequest();

                var token = document.querySelector('meta[name="csrf-token"]');

                token = (token ? token.getAttribute('content') : null);

                request.open('POST', fileUploadUrl, true);

                if (token) {
                    formData.append('authenticity_token', token);
                }

                request.onload = function() {
                    if (request.status >= 200 && request.status < 400) {
                        // Success!
                        var data = JSON.parse(request.responseText);

                        if (data.images) {
                            for (var i = 0; i < data.images.length; i += 2) {
                                var image = data.images[i];

                                var htmlString = '<div class="image-container"><span><img src="' + image.url + '" alt="image" /></span></div>';
                                dropper.insertAdjacentHTML('afterend', htmlString);
                            }
                        }
                    } else {
                        // We reached our target server, but it returned an error
                    }
                };

                request.send(formData);
            }
        }

        if (window.draggingElement) {
            window.draggingElement.classList.remove('dragged');
        }

        return false;
    });

    var editor_form = editor,
        editor_input;

    while (editor_form != document && editor_form.tagName.toLowerCase() != 'form') {
        editor_form = editor_form.parentNode;
    }

    if (editor_form.tagName.toLowerCase() == 'form') {
        editor_input = editor_form.querySelector('[data-wysiwyg=true]');

        editor_form.addEventListener('submit', function () {
            editor_input.innerHTML = editor.innerHTML;
        });

        editor.innerHTML = editor_input.childNodes[0].nodeValue;
    }
}