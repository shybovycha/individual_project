function sprintf(formatString) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    return formatString.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined' ? args[number].toString() : match;
    });
}
var HeartImageCropper = (function () {
    function HeartImageCropper(canvasSelector, imageUrl, allowEditing, offsetX, offsetY) {
        var _this = this;
        if (allowEditing === void 0) { allowEditing = true; }
        if (offsetX === void 0) { offsetX = 0; }
        if (offsetY === void 0) { offsetY = 0; }
        this.canvas = document.querySelector(canvasSelector);
        this.context = this.canvas.getContext('2d');
        this.imageObj = new Image();
        this.isDragging = false;
        this.originX = 350;
        this.originY = 0;
        this.sourceX = 350;
        this.sourceY = 0;
        this.originSourceWidth = 350;
        this.originSourceHeight = 350;
        this.scaleFactor = 1;
        this.sourceWidth = 350;
        this.sourceHeight = 350;
        this.destWidth = 350;
        this.destHeight = 350;
        this.destX = 0; // this.canvas.width / 2 - this.destWidth / 2;
        this.destY = 0; // this.canvas.height / 2 - this.destHeight / 2;
        this.sourceX = 0;
        this.sourceY = 0;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        // setup event listeners
        if (allowEditing) {
            this.canvas.addEventListener('mousedown', function (e) {
                _this.onMouseDown(e);
            });
            this.canvas.addEventListener('mousemove', function (e) {
                _this.onMouseMove(e);
            });
            this.canvas.addEventListener('mouseup', function (e) {
                _this.onMouseUp(e);
            });
        }
        this.loadSeasonAttribute();
        this.imageObj.onload = function () {
            _this.onImageLoaded();
        };
        // load image
        this.imageObj.src = imageUrl;
    }
    HeartImageCropper.prototype.setTransform = function (sourceX, sourceY, scaleFactor) {
        this.sourceX = sourceX;
        this.sourceY = sourceY;
        this.scale(scaleFactor);
    };
    HeartImageCropper.prototype.getTransform = function () {
        return {
            sourceX: this.sourceX,
            sourceY: this.sourceY,
            scaleFactor: this.scaleFactor
        };
    };
    HeartImageCropper.prototype.drawImage = function () {
        this.context.drawImage(this.imageObj, this.sourceX, this.sourceY, this.sourceWidth, this.sourceHeight, this.destX, this.destY, this.destWidth, this.destHeight);
    };
    HeartImageCropper.prototype.drawAttribute = function () {
        if (this.attributeImg) {
            this.context.drawImage(this.attributeImg, 0, 0);
        }
    };
    HeartImageCropper.prototype.loadSeasonAttribute = function () {
        var season = HeartImageCropper.getSeason();
        if (season == 'fall') {
            this.attributeImg = new Image();
            this.attributeImg.src = sprintf('/assets/realLeaf{0}.png', (Math.random() * 1000) % 7);
            this.attributeImg.onload = this.drawAttribute();
        }
    };
    HeartImageCropper.prototype.drawHeart = function () {
        this.context.save();
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.fillStyle = "rgba(0, 0, 0, 0)";
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.beginPath();
        this.context.moveTo(680 + this.offsetX, 259 + this.offsetY);
        this.context.bezierCurveTo(830 + this.offsetX, 140 + this.offsetY, 930 + this.offsetX, 397 + this.offsetY, 680 + this.offsetX, 500 + this.offsetY);
        this.context.bezierCurveTo(430 + this.offsetX, 400 + this.offsetY, 530 + this.offsetX, 137 + this.offsetY, 680 + this.offsetX, 259 + this.offsetY);
        this.context.bezierCurveTo(680 + this.offsetX, 259 + this.offsetY, 680 + this.offsetX, 259 + this.offsetY, 680 + this.offsetX, 259 + this.offsetY);
        this.context.closePath();
        this.context.stroke();
        this.context.lineWidth = 1;
        this.context.fill();
        this.context.clip();
        this.drawImage();
        this.context.restore();
        this.context.save();
        this.context.moveTo(680 + this.offsetX, 259 + this.offsetY);
        this.context.bezierCurveTo(830 + this.offsetX, 140 + this.offsetY, 930 + this.offsetX, 397 + this.offsetY, 680 + this.offsetX, 500 + this.offsetY);
        this.context.bezierCurveTo(430 + this.offsetX, 400 + this.offsetY, 530 + this.offsetX, 137 + this.offsetY, 680 + this.offsetX, 259 + this.offsetY);
        this.context.bezierCurveTo(680 + this.offsetX, 259 + this.offsetY, 680 + this.offsetX, 259 + this.offsetY, 680 + this.offsetX, 259 + this.offsetY);
        this.context.closePath();
        this.context.strokeStyle = "rgba(255, 140, 140, 1.0)";
        this.context.lineWidth = 12;
        this.context.shadowOffsetX = 4;
        this.context.shadowOffsetY = 2;
        this.context.shadowBlur = 5;
        this.context.shadowColor = "rgba(0, 0, 0, 0.5)";
        this.context.stroke();
        this.context.restore();
        this.drawAttribute();
    };
    HeartImageCropper.getSeason = function () {
        var month = new Date().getMonth();
        var season = 'unknown';
        switch (month) {
            case 12:
            case 1:
            case 2:
                season = 'winter';
                break;
            case 3:
            case 4:
            case 5:
                season = 'spring';
                break;
            case 6:
            case 7:
            case 8:
                season = 'summer';
                break;
            case 9:
            case 10:
            case 11:
                season = 'fall';
                break;
        }
        return season;
    };
    HeartImageCropper.prototype.scale = function (factor) {
        this.scaleFactor = factor;
        this.sourceWidth = this.originSourceWidth * this.scaleFactor;
        this.sourceHeight = this.originSourceHeight * this.scaleFactor;
        this.originWidth = this.imageObj.width - this.sourceWidth;
        this.originHeight = this.imageObj.height - this.sourceHeight;
        this.drawHeart();
    };
    HeartImageCropper.prototype.onImageLoaded = function () {
        this.originWidth = this.imageObj.width - this.sourceWidth;
        this.originHeight = this.imageObj.height - this.sourceHeight;
        if (this.imageObj.width < this.canvas.width) {
            this.destX = this.canvas.width / 2 - this.imageObj.width / 2;
        }
        if (this.imageObj.height < this.canvas.height) {
            this.destY = this.canvas.height / 2 - this.imageObj.height / 2;
        }
        var minSide = Math.min(this.originWidth, this.originHeight);
        var minSourceSide = Math.min(this.originSourceWidth, this.originSourceHeight);
        this.maxScaleFactor = parseInt((minSide / minSourceSide).toString()) + 2;
        this.drawHeart();
    };
    HeartImageCropper.prototype.getMousePos = function (evt) {
        var rect = this.canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
    };
    HeartImageCropper.prototype.onMouseDown = function (evt) {
        this.dragStartedAt = this.getMousePos(evt);
        this.dragStartedWith = { x: this.sourceX, y: this.sourceY };
        this.isDragging = true;
    };
    HeartImageCropper.prototype.onMouseMove = function (evt) {
        if (!this.isDragging)
            return;
        var startPos = this.dragStartedAt, mousePos = this.getMousePos(evt), delta = { x: startPos.x - mousePos.x, y: startPos.y - mousePos.y }, newPos = { x: this.dragStartedWith.x + this.scaleFactor * delta.x, y: this.dragStartedWith.y + this.scaleFactor * delta.y };
        if (newPos.x > this.originWidth)
            newPos.x = this.originWidth;
        if (newPos.x < 0)
            newPos.x = 0;
        if (newPos.y > this.originHeight)
            newPos.y = this.originHeight;
        if (newPos.y < 0)
            newPos.y = 0;
        this.sourceX = newPos.x;
        this.sourceY = newPos.y;
        this.drawHeart();
    };
    HeartImageCropper.prototype.onMouseUp = function (evt) {
        this.isDragging = false;
    };
    return HeartImageCropper;
})();
