function initMapEditor() {
    map.waypoints = map.waypoints || [];
    map.track = map.track || null;
    map.state = map.state || 'interacting';
    map.currentMarker = null;
    map.lastMousePos = map.getCenter();
    map.lastClick = null;

    function enableEditing() {
        map.getContainer().querySelector('.edit-mode-toggler').classList.add('toggled');
        map.state = 'editing';

        if (map.track) {
            map.track.addLatLng(map.lastMousePos);
        }

        map.currentMarker = L.marker(map.lastMousePos).addTo(map);
    }

    function disableEditing() {
        if (map.state == 'interacting') {
            return;
        }

        removeLastWaypoint();

        map.state = 'interacting';
        map.removeLayer(map.currentMarker);
        map.currentMarker = null;

        map.getContainer().querySelector('.edit-mode-toggler').classList.remove('toggled');
    }

    function removeLastWaypoint() {
        if (!map.track) {
            return;
        }

        var points = map.track.getLatLngs();

        points.pop();

        map.track.setLatLngs(points);

        if (!map.currentMarker) {
            map.removeLayer(map.waypoints.pop());
        }
    }

    var AddPointControl = L.Control.extend({
        options: {
            position: 'topright'
        },

        addPoint: function(evt) {
            evt.stopPropagation();

            if (this.classList.contains('toggled')) {
                disableEditing();
                map.doubleClickZoom.enable();
            } else {
                enableEditing();
                map.doubleClickZoom.disable();
            }
        },

        onAdd: function (map) {
            this.button = L.DomUtil.create('a', 'map-editor-control edit-mode-toggler');

            this.button.setAttribute('href', '#');
            this.button.innerHTML = '<span class="fa-map-marker"></span>';
            this.button.setAttribute('title', "Add waypoint");

            this.button.addEventListener('click', this.addPoint);

            return this.button;
        }
    });

    var RemoveLastPointControl = L.Control.extend({
        options: {
            position: 'topright'
        },

        removeLastPoint: function() {
            removeLastWaypoint();
        },

        onAdd: function (map) {
            this.button = L.DomUtil.create('a', 'map-editor-control');

            this.button.setAttribute('href', '#');
            this.button.innerHTML = '<span class="fa-undo"></span>';
            this.button.setAttribute('title', "Remove last waypoint");

            this.button.addEventListener('click', this.removeLastPoint);

            return this.button;
        }
    });

    function createMarker(pos, options) {
        if (!map.track) {
            map.track = L.polyline([ pos, pos ], { color: 'red' }).addTo(map);
        } else {
            map.track.addLatLng(pos);
        }

        var marker_options = {
            draggable: true,
            clickable: true,
            riseOnHover: true
        };

        if (!options) {
            options = {
                title: '',
                description: ''
            };
        }

        var marker = L.marker(pos, marker_options).addTo(map);

        marker.latLngIndex = map.waypoints.length;

        marker.on('drag', function(evt) {
            var points = map.track.getLatLngs();

            points[this.latLngIndex] = this.getLatLng();

            map.track.setLatLngs(points);
        });

        var popupHtml = '<input type="text" name="title" class="title" placeholder="Title" value="' + options.title + '" /><div contenteditable="true" class="editor description">' + options.description + '</div>';

        marker.bindPopup(popupHtml);

        marker.on('click', function() {
            map.currentEditingMarker = marker;

            if (marker.data) {
                document.querySelector('.leaflet-popup-content .title').value = marker.data.title;
                document.querySelector('.leaflet-popup-content .editor.description').innerHTML = marker.data.description;
            }

            /*[].forEach.call(document.querySelectorAll('.leaflet-popup-content .button'), function(elt) {
             elt.addEventListener('click', function() {
             var contentNode = marker.getPopup()._contentNode;

             marker.data = {
             title: contentNode.querySelector('.title').value,
             description: contentNode.querySelector('.editor.description').innerHTML
             };

             map.closePopup();
             });
             });*/
        });

        map.waypoints.push(marker);
    }

    map.addControl(new AddPointControl());
    map.addControl(new RemoveLastPointControl());

    map.on('mousemove', function(evt) {
        map.lastMousePos = evt.latlng;

        if (map.state == 'interacting' || !map.currentMarker) {
            return;
        }

        if (map.track) {
            var points = map.track.getLatLngs();

            points[points.length - 1] = map.lastMousePos;

            map.track.setLatLngs(points);
        }

        map.currentMarker.setLatLng(map.lastMousePos);
    });

    map.on('click', function(evt) {
        if (map.state == 'interacting' || !map.currentMarker) {
            return;
        }

        createMarker(map.lastMousePos)
    });

    map.on('popupclose', function(evt) {
        var contentNode = map.currentEditingMarker.getPopup()._contentNode;

        map.currentEditingMarker.data = {
            title: contentNode.querySelector('.title').value,
            description: contentNode.querySelector('.editor.description').innerHTML
        };

        map.closePopup();
    });

    // TODO: does not work properly. thus, needs reworks
    /*map.on('dblclick', function(evt) {
        disableEditing();
        map.doubleClickZoom.enable();
        return false;
    });*/

    document.addEventListener('keyup', function(evt) {
        if (evt.keyCode == '27') {
            disableEditing();
            map.doubleClickZoom.enable();
        }
    });

    document.querySelector('form').addEventListener('submit', function() {
        function addHidden(theForm, key, value) {
            // Create a hidden input element, and append it to the form:
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = key; // 'name-as-seen-at-the-server';
            input.value = value;
            theForm.appendChild(input);
        }

        debugger;

        var form = this;

        map.closePopup();

        map.waypoints.forEach(function(point) {
            if (point.data) {
                addHidden(form, 'points[][title]', point.data.title);
                addHidden(form, 'points[][description]', point.data.description);
            }

            addHidden(form, 'points[][lat]', point.getLatLng().lat);
            addHidden(form, 'points[][lng]', point.getLatLng().lng);
        });
    });

    if (window.tracks) {
        window.tracks.forEach(function(track) {
            track.points.forEach(function(point) {
                var pos = [ point.lat, point.lng ],
                    title = point.title || '',
                    description = point.description || '';

                createMarker(pos, { title: title, description: description });
            });
        });
    }
}