function setupPortrait(options) {
    if (!options.photo)
        options.photo = '/assets/couple.png';

    window.imageCropper = new HeartImageCropper('canvas.portrait-together', options.photo, false, -500, -200);

    window.imageCropper.setTransform(options.sourceX, options.sourceY, options.scaleFactor);
}

function initMap() {
    if (document.querySelectorAll('#map').length < 1) {
        console.error('Could not init map because no proper markup is found');
        return;
    }

    window.map = L.map('map').setView([51.505, -0.09], 13);

    // var tileUrl = 'http://api.tiles.mapbox.com/v4/shybovycha.j6oel6d1/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoic2h5Ym92eWNoYSIsImEiOiJpSklzTE9jIn0.UHrTYI5CaQcGaYPOC-NU1w';
    var tileUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

    L.Icon.Default.imagePath = '/assets';

    L.tileLayer(tileUrl, {
        attribution: '', //'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18
    }).addTo(map);
}

function scrollToRunner(to, duration) {
    if (duration <= 0)
        return;

    var body = document.querySelector('body');
    var difference = to - body.scrollTop;
    var perTick = difference / duration * 10;

    setTimeout(function() {
        body.scrollTop = body.scrollTop + perTick;
        if (body.scrollTop == to) return;
        scrollToRunner(to, duration - 10);
    }, 10);
}

function scrollTo(element, duration) {
    var to = element.offsetTop;

    scrollToRunner(to, duration);
}

function initPhotoDroppers(options) {
    var droppers = document.querySelectorAll('.photo-dropper');

    var dropper_config = {
        maxFileSize: (1024 * 1024 * 20),
        allowedFileTypes: [ 'image/png', 'image/gif', 'image/jpeg' ],
        fileUploadUrl: '/upload_images'
    };

    if (!options) {
        options = {
            mode: 'create'
        };
    }

    var their_canvas_selector = 'canvas.portrait-editor';

    if (options.mode == 'create') {
        document.querySelector(their_canvas_selector).parentNode.style.display = 'none';
    } else {
        // 'edit' mode
        var editorOptions = options;

        editorOptions['canvas_selector'] = their_canvas_selector;
        editorOptions['create_zoom_control'] = false;

        createPhotoEditor(editorOptions);
    }

    [].forEach.call(droppers, function(dropper) {
        var his_or_her = null;

        if (dropper.classList.contains('her'))
            his_or_her = 'her'; else
        if (dropper.classList.contains('his'))
            his_or_her = 'his'; else
        if (dropper.classList.contains('their'))
            his_or_her = 'their';

        var dropper_form = dropper;

        while (dropper_form.tagName.toLowerCase() != 'body' && dropper_form.tagName.toLowerCase() != 'form') {
            dropper_form = dropper_form.parentNode;
        }

        if (dropper_form.tagName.toLowerCase() != 'form') {
            dropper_form = null;
        }

        dropper.ondragover = function () { return false; };

        dropper.ondragend = function () { return false; };

        document.addEventListener('drop', function(e) {
            var maxFileSize = dropper_config.maxFileSize,
                allowedFileTypes = dropper_config.allowedFileTypes,
                fileUploadUrl = dropper_config.fileUploadUrl;

            if (e.target == dropper || e.target.parentNode == dropper) {
                e.preventDefault();

                var formData = new FormData();

                if (e.dataTransfer.files.length > 0) {
                    var file = e.dataTransfer.files[0];

                    if (allowedFileTypes.indexOf(file.type) < 0) {
                        console.error("could not upload file of type ", file.type, " while allowed types are only ", allowedTypes);
                        return false;
                    }

                    if (file.size > maxFileSize) {
                        console.error("could not upload file because its size exceeds the maximum allowed one - ", file.size, " out of ", maxFileSize);
                        return false;
                    }

                    formData.append('images[]', file);

                    var request = new XMLHttpRequest();

                    var token = document.querySelector('meta[name="csrf-token"]');

                    token = (token ? token.getAttribute('content') : null);

                    request.open('POST', fileUploadUrl, true);

                    if (token) {
                        formData.append('authenticity_token', token);
                    }

                    request.onload = function() {
                        if (request.status >= 200 && request.status < 400) {
                            // Success!
                            var data = JSON.parse(request.responseText);

                            if (data.images && data.images.length > 0) {
                                var htmlString = '<input type="hidden" value="' + data.images[0].id + '" name="' + his_or_her + '[photo_id]" />';
                                dropper_form.insertAdjacentHTML('beforeend', htmlString);
                                dropper.innerHTML = '<img src="' + data.images[0].url + '" alt="' + his_or_her + ' photo" />';
                                dropper.classList.add('no-background-image');

                                if (his_or_her == 'their') {
                                    // document.querySelector('img.resize-image').setAttribute('src', data.images[0].url);
                                    createPhotoEditor({
                                        photo: data.images[1].url,
                                        canvas_selector: their_canvas_selector,
                                        create_zoom_control: (options['mode'] == 'create')
                                    });
                                }
                            }
                        } else {
                            // We reached our target server, but it returned an error
                        }
                    };

                    request.send(formData);
                }
            }

            if (window.draggingElement) {
                window.draggingElement.classList.remove('dragged');
            }

            return false;
        });
    });
}

function createPhotoEditor(options) {
    var imageUrl = options['photo'];
    var their_canvas_selector = options['canvas_selector'];
    var createZoomControl = options['create_zoom_control'] || true;
    var sourceX = options['sourceX'] || 0;
    var sourceY = options['sourceY'] || 0;
    var scaleFactor = (options['scaleFactor'] || 1);

    var imageCropper = new HeartImageCropper(their_canvas_selector, imageUrl, true, -500, -200);

    window.imageCropper = imageCropper;
    imageCropper.setTransform(sourceX, sourceY, scaleFactor);

    document.querySelector(their_canvas_selector).parentNode.style.display = 'block';
    scrollTo(imageCropper.canvas, 500);

    imageCropper.canvas.addEventListener('mousemove', function() {
        if (!imageCropper.isDragging)
            return;

        document.querySelector('#their_source_x').value = imageCropper.sourceX;
        document.querySelector('#their_source_y').value = imageCropper.sourceY;
        // document.querySelector('[name=their[scale_factor]]').value = imageCropper.scaleFactor;
    });

    if (createZoomControl) {
        var slider = $('#scale-slider').CircularSlider({
            value: scaleFactor * 10,
            min: 10,
            max: 100,
            slide: function(_, value) {
                // slider.setRange(1, imageCropper.maxScaleFactor);

                value = value / 10.0;

                document.querySelector('#their_scale_factor').value = value;
                imageCropper.scale(value);
            },
            formLabel: function(value, prefix, suffix) {
                return "Zoom:" + parseInt(value / 10);
            }
        });
    } else {
        var slider = $('#scale-slider').CircularSlider();

        slider.setValue(scaleFactor * 10);
    }
}

function showTracks() {
    var bounds = null;

    window.tracks.forEach(function(track, idx) {
        var points = track.points.map(function(point) {
            var marker = L.marker([ point.lat, point.lng ], { clickable: true }).addTo(map);

            if (point.title || point.description) {
                var popupHtml = '<strong class="title">' + point.title + '</strong><div class="body">' + point.description + '</div>';

                marker.bindPopup(popupHtml);
            }

            return L.latLng(point.lat, point.lng);
        });

        var line = L.polyline(points, { color: track.color }).addTo(map);

        window.tracks[idx].line = line;
        window.tracks[idx].bounds = line.getBounds();

        if (bounds == null) {
            bounds = line.getBounds();
        } else {
            bounds.extend(line.getBounds());
        }
    });

    map.fitBounds(bounds);
}

function centerTrack(index) {
    if (index < 0 || index > window.tracks.length - 1)
        return;

    var bounds = window.tracks[index].bounds;

    //window.map.setView(bounds.getCenter());
    window.map.panInsideBounds(bounds);
}