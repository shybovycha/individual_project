require 'test_helper'

class CouplesControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  def setup
    @request.env["devise.mapping"] = Devise.mappings[:users]
    sign_in FactoryGirl.create(:admin)
  end

  test "should get show" do
    get :show
    assert_response :success
  end

  test "should register a couple" do
    post :create, { his: { password: "moofoo", password_confirmation: "moofoo", email: "moo@foo.bar",  }, her: {}, their: {} }
  end
end
