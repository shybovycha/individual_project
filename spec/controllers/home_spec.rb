require "rails_helper"

RSpec.describe HomeController, type: :controller do
  describe "Images uploader" do
    it "should upload single photo" do
      photo = fixture_file_upload('cat-acrobat-icon.png', 'image/png')

      class << photo
        attr_reader :tempfile
      end

      post :upload_images, images: [ photo ]

      expect(response).to be_success

      body_json = JSON.parse(response.body)

      expect(body_json.keys).to include('images')
      expect(body_json['images'].size).to eq(2) # original and medium images
      expect(body_json['images'].first).to include('url', 'id')
    end

    it "should upload multiple photo" do
      photo1 = fixture_file_upload('cat-acrobat-icon.png', 'image/png')
      photo2 = fixture_file_upload('cat-box-icon.png', 'image/png')

      class << photo1
        attr_reader :tempfile
      end

      class << photo2
        attr_reader :tempfile
      end

      post :upload_images, images: [ photo1, photo2 ]

      expect(response).to be_success

      body_json = JSON.parse(response.body)

      expect(body_json.keys).to include('images')
      expect(body_json['images'].size).to eq(4) # 2x original and medium images
      expect(body_json['images'].first).to include('url', 'id')
      expect(body_json['images'].last).to include('url', 'id')
    end
  end
end