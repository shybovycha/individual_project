require "rails_helper"

RSpec.describe CouplesController, :type => :controller do
  describe "Registration" do
    it "responds successfully with an HTTP 200 status code" do
      stub_model Photo, id: 1
      stub_model Photo, id: 2
      stub_model Photo, id: 3

      his_params = { password: "moofoo", password_confirmation: "moofoo", email: "moo@foo.bar", photo_id: 1 }
      her_params = { password: "foomoo", password_confirmation: "foomoo", email: "foo@moo.bar", photo_id: 2 }
      their_params = { photo_id: 3 }

      post :create, { his: his_params, her: her_params, their: their_params }

      expect(response).to be_success
    end
  end

  describe "Login" do
    it "should log in couple via one of the members" do
      # @request.env["devise.mapping"] = Devise.mappings[:user]
      # user = FactoryGirl.create(:user)
      # user.confirm! # or set a confirmed_at inside the factory. Only necessary if you are using the "confirmable" module
      # sign_in user
    end

    #it "renders the index template" do
      #get :index
      #expect(response).to render_template("index")
    #end
  end
end