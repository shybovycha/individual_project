# They're Trippin'

## Brief Overview

This is a project for couples who like to travel and want to share their stories or create their own adventure storybook.

It allows to create a story, with a map and a set of photos. And that's all! The storybook is ready!

## Main Highlights

* Sign up as a couple, sign in as a single person
* Create map with tracks, where each point can handle a photo
* Write an exciting story with even more shots

## Detailed Overview

The project is aimed to couples who like to travel and want to share their stories or create their own adventure storybook.
It allows to create a story, with a map, containing a trip' route and a set of photos, and publish it. These "storybooks",
or "blogs" are published on a website and available to any guest users or site visitors. Each track on a map consists of
waypoints, each can contain a photo with a short description. Story contains photos and lot of text as well. Each story
could be tagged with a bunch of tags, describing the trip. Tag consists of a title and an icon. Tags are pre-set.
Users can not create their own. Good examples for a tag are: "hiking", "biking", "beach", "city walk". Story could be tagged
as "done alone" or "done in couple".

Each couple has its own page, showing the map with all the routes gathered together. Each track has its own color. Under the
map there is be a list of all the couple' stories. Each story in that list has the same color as its track. Stories in that
list are displayed as a short description.

Each couple signs up as two users, but when someone in the couple decides to write a story, he/she signs in using his/her
personal credentials. Then, additional controls on a couple page appear, like "create a new story", "edit story",
"delete story". When editing/creating a story, the WYSIWYG editor appears along with a photo upload box, where all
the story photos are stored. Photos from that box could be drag-and-dropped to a story. Or this feature may be bundled
in WYSIWYG editor. Along with a WYSIWYG editor, map editor is loaded, where a track could be created. Track is created
as a polyline on a map, defined by a set of markers-waypoints. When a marker is selected, it may be either moved, or deleted,
or set a photo with a description. The short description for a story is marked in WYSIWYG editor with a `<cut>` tag.

## Detailed Features

* Signing up as two users, bundled into one structure, called "couple"
* Signing in as a single user
* CRUD actions for a blog post (C/U/D for signed in users only, R for everyone), called "story"
* When creating a story, a map and a set of photos are assigned to one
* Track is assigned to a post and should be created in a map editor
* Stories should be assigned a set of tags, declaring a story type (symbolic story description)